package blockchain

import (
	"encoding/binary"
)

// uint64 to byte array
func Uint64ToByteArray( v uint64 ) [ ]byte {
	out := make( [ ]byte,
		8 )
	binary.BigEndian.PutUint64( out,
		v )
	return out
}


// uint32 to byte array
func Uint32ToByteArray( v uint32 ) [ ]byte {
	out := make( [ ]byte,
		4 )
	binary.BigEndian.PutUint32( out,
		v )
	return out
}
