package main

import (
	"./blockchain"
	"fmt"
	"time"
)

// entry point
func main( ) {
	b := &blockchain.Block{
		Target:    0x00000FFFFFFFFFFF,
		Timestamp: uint64( time.Unix( 0, 0 ).Unix( ) ),
		Data:      &blockchain.Data{ Word: [ ]string{ "test", "xptdr" } },
	}
	t := time.Now( )
	b.Mine( )
	fmt.Println( "Took",
		time.Now( ).Sub( t ).Seconds( ),
		"seconds to mine and find nonce=",
		b.Nonce )
}
